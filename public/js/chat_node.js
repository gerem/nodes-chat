﻿// Area for button actions
// On Ready

/*iniciamos la comunicacion con el server para recibir datos*/
socket.emit('say.hello', {token : localStorage.getItem('token')});

/*Acciones basicas de chat*/

socket.on('profile.success', function ( data ) {
    console.log(data);
});

socket.on('login.success', function ( data ) {
	//Asignamos el nickname
	myNick = data.nick;
	//Guardamos el token
    localStorage.setItem("token", data.token);

    //Limpieza de interfaz
    $('#nick-dom').html( myNick );
    $('#logout-btn').css('display','inline');
    $('#main-login').css('display','none');

    //Obtenemos chats de cliente

    alert('Has iniciado sesión !');
});

socket.on('client.is.off' , function( data ){
	//Notificamos al usuario
	//addClient(data);
	alert( "Usuario desconectado" );
})

socket.on('client.is.change' , function( data ){

	//Notificamos al usuario
	//addClient( data );//Arreglo de usuarios
	alert( "Usuario ha cambiado de nick" );
})

socket.on('new.user', function( data ){
	console.log( data )

	//addClient(data);
})

/*
	El usuario se ha desconectado
 */
socket.on('you.are.offline',function( data ){
	//Eliminamos el token
	localStorage.removeItem('token');
	//Notificamos al usuario
	alert( "Haz cerrado sesión correctamente" );
	//Limpamos la interfaz
	cleanInterface();
})

socket.on('session.is.expired',function( data ){
	console.log('Token removido por expiración')
	//Eliminamos el token
	localStorage.removeItem('token');
})

/* Errores de conexion con socket's */
//Data with socket's
socket.on('profile.error', function ( data ) {
    console.log(data);
});

socket.on('user.logout',function( data ){
	//Eliminamos el token
	localStorage.removeItem('token');
	//Limpiamos la interface
	cleanInterface();
	//Notificamos los datos de error al cliente
	alert(data);
})

socket.on('room.was.create', function( data ){
	alert('La sala fue creada anteriormente');
	socket.emit('sign.in.room', data );
})

socket.on('name.is.used' , function( data ){
	alert('El nombre no esta disponible o cuenta con guion medio');
})

socket.on('user.is.disconnected' , function( data ){
	//addClient( data );
})

socket.on('clients.data', function( data ){
	console.log('->',data)
	//Levantamos el modal con usuarios
	addClient( data );
	//Servimos los datos de usuarios
	//Esperamos la creacion( un solo usuario )
})

socket.on('new.rooms', function( data ){
	console.log('Mostrar salones...\n',data);
	addRooms( data );
});

socket.on('error.w',function( data ){
	alert( data );
});

socket.on('new.room.created',function( data ){
	console.log('Invitación a chat [ok]');
	socket.emit('sign.in.room',data);
})

socket.on('data.chat',function( data ){
	renderChat( data );
})

socket.on('new.msg.room',function(data){
	/*
	nick : decoded.nick,
	room : ""
	messages : rooms[ index ].messages
	*/
	if( data.nick != myNick ){
		$('#state-' + data.room).html('new_releases');
	}
	if(current_chat == data.room){
		data.nick = myNick;
		renderChat( data );
	}
})

socket.on('review.rooms',function( data ){
	console.log('actualizando rooms', data)
	socket.emit('update.rooms',{
		token:localStorage.getItem('token'),
		room : data
	})
})

//Funciones del sistema
function send_msg(){
	console.log('enviando msg',current_chat)
	var text = $('#message').val();

	socket.emit('send.msg', { 
		room  : current_chat,
		text  : text,
		token : localStorage.getItem('token')
	});
}

function login() {

    var data = {
        nick: $('#nick').val(),
        email: $('#email').val(),
        token : localStorage.getItem('token')
    };

    socket.emit('login' , data );
}

function logout() {
    socket.emit('logout', {token: localStorage.getItem('token')});
}

function createChat(){
	console.log('Solicitud de chat')
	//Elementos
	socket.emit('create.room',{ 
		target : selected_user ,
		token  : localStorage.getItem('token')
	});
}
