/**
	Funciones útilies para el sistema 
 */

 $(document).ready(function() {

	$('.modal').modal();
	// Modal button -> Create chat
	$('#create-btn').click( createChat );

	$('#newchat').click(function(e){
		
		$('#chats-modal').modal('open');
		socket.emit('get.users' , { token : localStorage.getItem('token')})

	});

	//Evento de teclado para envio de mensajes
	$('#message').keydown(function( event ){
		if ( event.which == 13){//Detectamos salto de linea
			//Funciones del sistema
			console.log('enviando msg',current_chat)
			var text = $('#message').val();

			socket.emit('send.msg', { 
				room  : current_chat,
				text  : text,
				token : localStorage.getItem('token')
			});
		}
	})

	$('#broadcast-General').click(selectChat);
	// Validation form with HTML5
	$('#auth-form').on('submit', function(event) {
		// Prevent default action of the form
		event.preventDefault();

		login();
	});
});

function addClient( list ){

	var target = $('#list_users');

	target.empty();//Limpiamos el DOM

	list.forEach((client, index) => {
		
		if(client.nick != myNick)
			var data = getTemplateClient(client.id, client.nick, client.email, client.state);

		target.append( data );

		$('#' + client.id).click(selectUser);
	});
}

function cleanInterface() {
	$('#logout-btn').css('display','none');
    $('#main-login').css('display','flex');
	$('#list_users').empty();
	$('#list_chats').empty();
}

function getTemplateClient( id , nick , email , state ) {

	var state_user = (state) ? "Connected" : "Disconnected";

	return 	"<div id='" + id + "' class='client col s4 m4 l4'>" + 
			    "<div class='card white' style='cursor: pointer'>" +
			        "<div class='card-content black-text'>" +
			            "<span class='card-title truncate'>" + nick + "</span>" +
			            "<p class='truncate'>" + email + "</p>" +
			            "<strong><p>" + state_user + "</p></strong>" +
			        "</div>" +
			    "</div>" +
			"</div>";
}


function getTemplateRoom( nameServer , nick ){
	var name = getNameChat( nameServer , nick );
	return 	"<div class='transparent waves-effect waves-light' style='width:100%'>" + 
				"<div class='chat-room' id='" + nameServer + "' >" +
						"<h5>" + name + "</h5>" +
						"<span class='icon-room'>" +
							"<i id='state-" + nameServer + "' class='material-icons'>done</i>" + 
						"</span>" +
				"</div>" + 
			"</div>";
}

function getNameChat( nameServer , nick ){
	var names = nameServer.split('-');
	return (names.indexOf( nick ) == 0)? names[1] : names[0];
}

function addRooms( data ){
	rooms = [];//Limpiamos las habitaciones
	var target = $('#list_chats');

	target.empty();//Limpiamos DOM
	target.append( getTemplateRoom( 'broadcast-General' , 'broadcast' ) );
	$('#broadcast-General').click(selectChat);//Event

	rooms = data.rooms.map( ( room ) => {
		
		target.append( getTemplateRoom( room.name , data.nick ) );
		$('#' + room.name ).click( selectChat );//Listener

		return {name : room.name, messages : 0};
	});
}

function selectUser( e ){
	//Seleccionamos y obtenemos id de elementos
	console.log(e.currentTarget.id)
	if( selected_user != '' ){

		$('#' + selected_user).css('background','white')
		$('#' + e.currentTarget.id).css('background','#aed581')
		selected_user = e.currentTarget.id;

	}else{
		
		$('#' + e.currentTarget.id).css('background','#aed581')
		selected_user = e.currentTarget.id;
	}
	
}


function selectChat( e ){
	console.log('Generamos transición entre salas');
	
	//Seleccionamos y obtenemos id de elementos
	current_chat = e.currentTarget.id;

	//Obtener la informacion del chat
	socket.emit('get.data.chat',{
		token : localStorage.getItem('token'),
		room : current_chat
	})
}

function getTemplateMsg( msg , nick ){
	var kind = ( nick != msg.owner )? ['my','start'] : ['his','end'];
	return 	"<div class='flex f-" + kind[1] + " his'>" +
				"<div class='" + kind[0] + "-msg'>" +
					"<span>" + msg.txt + "</span>" +
				"</div>" +
				"<div class='" + kind[0] + "-tail'></div>" + 
			"</div>";

}

function renderChat( data ){
	//Colocamos nombre de chat

	$('#current_chat').html( getNameChat(current_chat , data.nick) );
	$('#state-' + data.room).html('done');
	//Renderizamos la data del chat
	var content = $('#chat_content');
	content.empty();

	data.messages.forEach( (msg) =>{
		content.append(getTemplateMsg( msg , data.nick ));
	})
	
}