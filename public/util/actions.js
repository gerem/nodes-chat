/*

socket -> [id , room , session ?]

function recoverySession( socket , data ){

	//Token -> datos de usuario encriptados
	jwt.verify( data.token , SEC , function( err , decoded ){
		let index = getClientByToken( data.token );//Poiscion

		if( err ){//La session ha expirado
			//Los datos se han perdido -> genera basuara en el server
			//Limpiar los datos del usuario

			if( index != -1){
				//Obtenemos el token y con la referencia de desconexión
				//borramos los datos
				console.log(`El usuario: ${clients[index].nick} ha cerrado sesión`)
				clients.splice(index,1);

				//io.sockets.emit('client.is.off');
				emitLoginUsers( 'client.is.off' , clients );
			}else{
				socket.emit('user.error.logout', `El usuario no esta registrado`);
			}
			socket.emit('session.is.expired');
		}
		if( index != -1){
			//Activamos los datos de usuario
			let client = clients[index];
			client.state = true;

			//Eliminamos los datos de usuario
			clients.splice(index,1);
			
			loginClient( socket , client );
		}else{ socket.emit('session.is.expired') }
		
	});
	return false;
}

function loginWithToken( socket , data ){
	//Token -> datos de usuario encriptados
	jwt.verify( data.token , SEC , function( err , decoded ){
		let index = getClientByToken( data.token );//Poiscion

		if( err ){//La session ha expirado
				//Los datos se han perdido -> genera basuara en el server
				//Limpiar los datos del usuario

			if( index != -1){
				//Obtenemos el token y con la referencia de desconexión
				//borramos los datos
				console.log(`El usuario: ${clients[index].nick} ha cerrado sesión`)

				let client = {
					nick  : data.nick,
					email : data.email,
			        state : true,
			        token : "",
			        rooms : clients[index].rooms,
			        id    : ""
				}
				//Eliminamos los datos de usuario
				clients.splice(index,1);

				loginClient( socket , client );

			}else{
				socket.emit('user.error.logout', `El usuario no esta registrado`);
				socket.emit('session.is.expired');
			}
			return;
		}
		//El token aun se encuentra activo
		if( index != -1){

			let client = {
				nick  : data.nick,
				email : data.email,
		        state : true,
		        token : "",
		        rooms : clients[index].rooms,
		        id    : ""
			}
			client.state = true;

			//Eliminamos los datos de usuario
			clients.splice(index,1);
			loginClient( socket , client );
			return;
		}else{ socket.emit('session.is.expired') }
	});
	return false;
}


<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=full, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>NodeSocket's</title>

    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css">

    <!-- JavaScript - Before -->
    <script src="/socket.io/socket.io.js"></script>
</head>

<body class="row fondo">
    <div class="col s12 m4 l3 sala">
        
        <h3 class="center">
            <p class="p1">Nodes/ </p>
            <p class="p2">Chat</p>
        </h3>

        <div class="divider"></div>

        <!-- This opens a modal - chats-modal -->
        <div id="openModal" class="newchat center">
            <span id="newchat">
                Crear chat
                <i class="material-icons right">add</i>
            </span>
        </div>

        <!-- Lista de chats -->
        <div class="userchats">
            <div id="list_chats" class="content-clients">
                <div id="broadcast-General" class="chat">
                    <h5>Public Room</h5>
                    <i id="state-broadcast-General" class='material-icons'>done</i>
                </div>
            </div>
            <div class="divider">   </div>
        </div>
    </div>

    <div class="col s12 m8 l9">

        <button class="waves-effect waves-light btn botones red" id="logout-btn" style="display: none; width: 100%;" onclick="logout()">
            <i class="material-icons right">person_outline</i>
            LogOut
        </button>

        <div id="chat_box" class="chat"> 
            <div id="chat" class="col s12 m12 l12" style="height: 80vh;">

                <h5 class="difuminado">
                    <p class="p1 col s2 m2 l1">Chat#</p>
                    <p id="current_chat" class="p4 col s8 m8 l8">General</p>
                </h5>
                <div id="chat_content" class="scrollspy flex"></div>
            </div>

            <div class="col s12 m12 l12 inputs" style="height: 15vh;">
                <div class="input-field col s12 m8 l10">
                    <input id="message" type="text">
                    <label for="message">Escribe tu mensaje</label>
                </div>
                <button class="btn waves-effect waves-light col s12 m2 l2 botonenviar" onclick="send_msg()" type="submit" name="action">Send
                    <i class="material-icons right">send</i>
                </button>
            </div>
            
        </div>
    </div>
    
    <div id="main-login">
        <h3 class="white-text">Iniciar sesión</h3>
        <form id="auth-form">
            <div class="input-field">
                <input class="white-text" type="text" id="nick" required="required">
                <label for="nick">Nickname</label>
            </div>
            <div class="input-field">
                <input class="white-text" type="email" id="email" required="required">
                <label for="email">E-mail</label>
            </div>
            <div class="">
                <button type="submit" class="blue waves-effect waves-light btn botones">
                    <i class="material-icons right">person</i>
                    LogIn
                </button>
            </div>
        </form>
    </div>

    <!-- I'm the chats-modal -->
    <div id="modal1" class="modal">

        <div class="modal-content black">
            <h4>Agregar un chat</h4>
            <p>Selección de usuario para nuevo chat</p>
        </div>

        <div id="list_users"></div>

        <div class="modal-footer">
            <button id="create-btn" 
                    class="modal-action modal-close waves-effect waves-green btn-flat">
                Crear
            </button>
        </div>
    </div>
    

    <!-- JavaScript - After -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/socket.js"></script>
    <script src="js/render.js"></script>
    <script src="js/chat_node.js"></script>

</body>
</html>

*/