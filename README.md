# Node's Chat

Aplicación tipo chat para comunicación entre usuario por medio de sessiones. Los usuario pueden comunicarse por medio de broadcast y por medio de mensajes directos.

## Requsistos
* Node LTS 6.9.*
* IDEA || TextEditor
* nodemon

### Ejecución

Ejecución de servidor principal con node
```
npm start
```

Ejecución con nodemon
```
npm run nodemon
```