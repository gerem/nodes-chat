'use strict';

let server = require('../main');

//Configuracion de puertos
const PORT  = 8080;

//Importante usar SERVER en lugar de app
//server mantiene la conexion entre socket io y el cliente
server.listen( PORT , () => console.log( `Servidor corriendo en ${PORT}` ));