'use strict';

//Contantes empleadas a lo largo del servidor
const   express = require('express'),
        SEC = "_La*w3alAmo$sect4w3_0NNna",
        TOKEN_TIME = '60m';

//Almacenamiento de datos de clientes

let clients = [];

let rooms = [
	{
		name : "broadcast-General",
		clients : ["all"],//NO RENOMBRAMIENTO
		messages : []
	}
];

/*
	client = {
		"nick":"nickname",
		"email":"mail@hotmail.com",
		"state":true,
		"token":"token_as",
		"rooms":[]
	}

	room = {
		name : "Nick1&Nick2",
		clients : ["nick_1","nick_2"],//NO RENOMBRAMIENTO
		messages : [
			{
				owner : "NICK",
				datetime : "", 
				txt : ""
			}
		]
	}
*/

//Importancion de librerias.
let app    = express(),
	server = require('http').Server( app ),
	jwt    = require('jsonwebtoken'),//Wea segura
	io     = require('socket.io')(server),
	router = require('./Http/Router');

//Carpeta estatica para los assets
app.use(express.static('public'));
app.set('view engine', 'pug')

//Definicion de archivos HTTP
app.use('/' , router );
app.get('/users', function( req , res ){//Verificacion de usuarios
	res.send({data : clients})
})

//Funcion de escucha eventos en sockets de clientes
io.on('connection' , function( socket ){
	//Evento solicitado para la conexion inicial
	//emit y on
	socket.on('say.hello',function( data ){
		if(data.token != undefined){
			//Socket , data , si la sesion->true se regenera o restablece->false
			clientToken( socket , data , false );
		}
	})

	socket.on('send.msg',function(data){

		//HTML injection :'v
		if(/<s*(.*?)>/g.test( data.text )){
			socket.emit('error.w',"Mesaje malicioso prro :'v");
			return;
		}


		//Datos nulos o corruptos
		if(data.text === "" || data.text.length > 255) {
			socket.emit('error.w',"El mensaje esta vacio o excede el numero de caracteres")
			return;
		}

		try {//Verificacion de token
			var decoded = jwt.verify( data.token , SEC );
				
			let index = getRoom( data.room );
			if( index != -1){
				
				//Obtenemos la sala
				rooms[index].messages.push({//Insertamos los datos 
					owner : decoded.nick,
					datetime : Date(), 
					txt : data.text
				})
				
				let data_room = {
					nick : decoded.nick,
					room : rooms[ index ].name,
					messages : rooms[ index ].messages
				}

				//Es un broadcast?
				if( data.room === 'broadcast-General' ){
					emitLoginUsers( 'new.msg.room' , data_room );
				}else{
					//Notificamos a usuarios en room
					io.to(rooms[index].name).emit('new.msg.room' , data_room );	
				}
				
				socket.emit('data.chat' , data_room);
			}else{
				socket.emit('error.w',"Error al enviar el mensaje")
			}

		} catch(err) {
			console.log('ERROR\n' , err ,"\n:",data)
			//recoverySession( socket , data )
			clientToken( socket , data , false );
		}
	})

	//Envento para el envio de mensajes entre usuarios y salas
	socket.on('create.room', function( data ){

		console.log('creating room:\n',data)

		try {//Verificacion de token
			var decoded = jwt.verify( data.token , SEC );

			let A = getClient( socket.id , 'id' );
			let B = getClient( data.target , 'id' );

			//Existe un chat con estos miembros (id's)
			let index = roomCreated( A , B )//?
			
			if( index == -1 ){//Encontramos salon ?
				
				let nickA = clients[A].nick,
					nickB = clients[B].nick;

				//Cargamos los datos del salon
				let room = {
					
					name : nickA + "-" + nickB,
					clients : [nickA,nickB],
					messages : []
				}

				//Agregamos la sala
				rooms.push( room );

				//Agregamos usuarios 
				socket.join( room.name );
				io.sockets.connected[data.target].join( room.name );

				//Notificamos a los usuarios
				io.to(room.name).emit('new.room.created' , room.name );

				console.log('\n--->Datos de habitación creados<---\n');
			}else{
				//Retornamos el chat 
				socket.emit('room.was.create');
			}

		} catch(err) {
			console.log('ERROR\n' , err)
			//recoverySession( socket , data )
			clientToken( socket , data , false );
		}
	})

	socket.on('sign.in.room', function( data ){
		
		console.log('\nAgregando datos de salon a cliente\n');

		let index = getClient( socket.id , 'id' );

		if( index != -1 ){

			let room = rooms[ getRoom( data ) ]

			//Agregamos el salon
			clients[index].rooms.push( room );

			//Enviamos datos para render
			socket.emit('new.rooms', {
				rooms : clients[index].rooms,
				nick : clients[index].nick
			});
			
		}else{	
			socket.emit('error.w',"El usuario no existe")
		}
	})

	socket.on('get.data.chat', function( data ){

		try {//Verificacion de token
			var decoded = jwt.verify( data.token , SEC );
				
			let index = getRoom( data.room );
			if( index != -1){
				//Retornamos las conversaciones 
				//socket.emit('data.chat', rooms[ index ].messages );
				socket.emit('data.chat',{
					nick : decoded.nick,
					room : rooms[ index ].name,
					messages : rooms[ index ].messages
				});
				
			}else{
				socket.emit('error.w',"Sala no encontrada o corrompida")
			}

		} catch(err) {
			console.log('ERROR\n' , err ,"\n:",data)
			//recoverySession( socket , data )
			clientToken( socket , data , false );
		}

	})

	//Obtiene los datos del usuario. y los envia
	socket.on('profile' , ( data ) => { 
		getProfile( socket , data );
	});

	//Iniciamos sesion con los datos del usuario que recibimos de Front
	socket.on('login' , function( data ){
		
		if(! isTheNameFree( data.nick ) ){//?
			socket.emit('name.is.used');
			return false;
		}

		//Usuario tiene un token -> Recuperar sesion
		if( data.token != null){
			//Recuepramos la session
			//loginWithToken( socket , data );
			clientToken( socket , data , true );
			return false;
		}

		let client = {
			nick  : data.nick,
			email : data.email,
	        state : true,
	        token : "",
	        rooms : [],
	        id    : ""
		}

		loginClient( socket , client );
	});

	socket.on('get.users', function( data ){
		try {

			var decoded = jwt.verify( data.token, SEC );
			socket.emit('clients.data', clients );

		} catch(err) {
			//recoverySession( socket , data )
			clientToken( socket , data , false );
		}
	});

	socket.on('update.rooms',function(data){
		console.log('LOG rooms user')
		try {//Verificacion de token
			var decoded = jwt.verify( data.token , SEC );
				
			let index = getClient( socket.id , 'id' );

			let index_room = clients[index].rooms.indexOf( data.room );
			clients[index].rooms.splice(index_room,1);

			if( index != -1){
				socket.emit('new.rooms', { 
					rooms : clients[index].rooms,
					nick : clients[index].nick 
				});
			}else{
				socket.emit('error.w',"Sala no encontrada o corrompida")
			}

		} catch(err) {
			console.log('ERROR\n' , err ,"\nupdate rooms:")
			//recoverySession( socket , data )
			clientToken( socket , data , false );
		}
	})

	socket.on('disconnect',function( data ){
		//sokect de desconexion
		//getClient -> -1 indica error de busqueda
		let index = getClient( socket.id , 'id' );

		if( index != -1 ){
			clients[index].state = false;
			emitLoginUsers('user.is.disconnected', clients);
		}
	})

	//Cerrar session de usuario
	socket.on('logout' , function( data ){
		removeClient( socket , data );
	});

});


function roomCreated( A , B ){
	for (var i = 0; i < rooms.length; i++) {
		let curr = rooms[i].clients;
		if( curr.indexOf( A ) != -1  && curr.indexOf( B ) != -1 ){
			return i;
		}
	}
	return -1;
}

/**
	Propagación de datos a todos los usuarios con sesion
	@param : action : string : Tipo de accion
	@param : data : Object : Objeto con datos de usuario
 */
function emitLoginUsers( action , data ){
	clients.forEach( client =>{
		try{
			if(client.state != false)
				io.sockets.connected[client.id].emit( action , data );
		}catch( err ){ 
			console.error( err , "EMIT LOGIN USERS") 
		}
	})
}

/**
	Inicio de sesion de cliente
	@param : socket : objetct : objeto con los datos y metodos de socket
	@param : data : Object : Objeto con datos de usuario
 */
function loginClient( socket , data ){
	let client = {
		nick  : data.nick,
		email : data.email,
		state : data.state, //Activo o desctivado
		token : "",
		rooms : data.rooms,
		id    : socket.id
	}

	//Defnicion de duracion
	let token = jwt.sign( client , SEC , {
		//Expira la session en 60 minutos
		expiresIn : TOKEN_TIME
	});

	//Ageregamos el valor al usuario
	client.token = token;

	//Guardamos los datos de nuevo cliente
	clients.push( client );

	socket.emit('login.success', { token : token , nick : data.nick });
	socket.emit('new.rooms', { rooms : client.rooms,nick : client.nick });
	console.log( "Los datos guardados.\n" , client);

	//io.sockets.emit('new.user', visible_data );
	//Enviamos los datos del chat al usuario
	emitLoginUsers( 'new.user' , client.nick );
}

/**
	Renovacion o creacion de nueva sesion para usuarios via token o nick
	@param : socket : objetct : objeto con los datos y metodos de socket
	@param : data : Object : Objeto con datos de usuario
	@param : generate : Boolean : tipo de accion renovacion o creacion
 */
function clientToken( socket , data , generate ){
	
	let index = getClient( data.token , 'token' );//Poiscion

	// invalid token - synchronous
	try {
		var decoded = jwt.verify( data.token, SEC );

		if( index != -1){
			//Datos de usuario local
			let current = clients.slice(index,index + 1);
			
			//Borrar datos de cliente
			clients.splice(index,1);

			//Enviamos los salones
			data.rooms = current[0].rooms;
			//Seleccion de modalidad de inicio de datos
			let client = (generate)? data : current[0];
			//Reconectamos usuario con salas en caso de reconexion
			if( ! generate ){
				client.rooms.forEach( room =>{
					console.log(room , '<-- ROOM')
					try{
						socket.join( room.name );
					}catch(err){
						console.log(err,"RE-JOIN SOCKET")
					}
				});
			}
			setupUser( socket , client );
			
		}else{socket.emit('session.is.expired') }

	} catch(err) {
		console.error(err,"ERROR CLIENT TOKEN")
		//token caducado o erroneo
		if( index != -1){
			//Log en server
			console.log(`Un usuario ha cerrado sesión`)
			//borramos los datos
			clients.splice(index,1);
		}

		if( generate )
			setupUser( socket , data );
		else
			socket.emit('session.is.expired');
	}
	return;
}

/**
	Configuracion de datos pre registro
	@param : socket : objetct : objeto con los datos y metodos de socket
	@param : data : Object : Objeto con datos de usuario
 */
function setupUser( socket , data ){
	let client = {
		nick  : data.nick,
		email : data.email,
        state : true,
        token : "",
        rooms : data.rooms,
        id    : ""
	}
	loginClient( socket , client );
}

/**
	Envio de datos de usuario por medio de sockets
	@param : socket : objetct : objeto con los datos y metodos de socket
	@param : data : Object : Objeto con datos de usuario
 */
function getProfile( socket, data ){
	jwt.verify( data.token , SEC , ( err , decoded ) => {
		if( err )
			socket.emit('profile.error', err );
		
		socket.emit('profile.success', decoded );
	});
}

/**
	Eliminar datos de un cliente
	@param : socket : objetct : objeto con los datos y metodos de socket
	@param : data : Object : Objeto con datos de usuario
 */
function removeClient( socket , data ){
	//Obtenemos datos del cliente
	let index = getClient( data.token , 'token' );

	//Token corrompido
	if( data.token == null || undefined ){
		socket.emit( 'user.logout', `Error al cerrar la sessión, Sesión corrompida`);
		return false;
	}

	if( index != -1 ){
		console.log('salas actualizadas')


		clients[index].rooms.forEach( room =>{
			//Cerramos canal de comunicacion
			let index_room = getRoom( room.name );//Obtenemos room
			let current = rooms.slice(index_room,index_room + 1);//Temp
			rooms.splice(index_room,1);//Eliminamos
			
			console.log(current,"\nDATA \n",index_room,"\n")
			//Notificamos
			io.to(current[0].name).emit('review.rooms',current[0].name);
		});


		//Eliminamos los datos de usuario
		clients.splice(index,1);
		//Notificamos a los usuarios
		socket.emit('user.logout', `La sesión se ha cerrado`);
		emitLoginUsers('client.is.off', clients );
	}else
		socket.emit('user.logout', `Error de sesión, Usuario no registrado`);
}

/**
	Obtenemos los datos del cliente
	@param : Compare : <T> Generico el dato a comparar
	@param : attr : String llave de busqueda con usuario.
 */
function getClient( compare , attr ){//Obtener elementos
	for (var i = 0; i < clients.length; i++) {
		if (clients[i][attr] == compare)
			return i;
	}
	return -1;
}

/**
	Obtenemos los datos del cliente
	@param : name : String Nombre de room
	@return : int : con posición de room
 */
function getRoom( name ){
	for (var i = 0; i < clients.length; i++) {
		if (rooms[i].name == name )
				return i;
	}
	return -1;
}

/**
	Verificacion de disponibilidad de nick name
	@param : nick : String : nombre a comparar 
 */
function isTheNameFree( nick ){//?
	return ( getClient( nick , 'nick') == -1 && nick.split('').indexOf('-') == -1);
}

//Exportamos la aplicacion
module.exports = server;